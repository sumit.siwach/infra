module "VPC" {
  source  = "./modules/VPC"
  vpccidr = var.vpccidrout
  vpcname = var.vpcnameout
}


module "Subnet" {
  source   = "./modules/SUBNET"
  vpc_id   = module.VPC.vpcid
  vpc_cidr = module.VPC.vpccidr
}

module "igw-ngw" {
  source        = "./modules/GATEWAY"
  vpc_id        = module.VPC.vpcid
  pub_subnet_id = module.Subnet.public_subnet_id[1]
  eip_id        = module.igw-ngw.eip_id
}


module "route-table" {
  source        = "./modules/ROUTE-TABLE"
  vpcid         = module.VPC.vpcid
  igwid         = module.igw-ngw.igw-01_id
  ngwid         = module.igw-ngw.ngw-01_id
  pub_subnet_id = module.Subnet.public_subnet_id
  rt-pub_id     = module.route-table.rt-pub_id

  pvt_subnet_id = module.Subnet.private_subnet_id
  rt-pvt_id     = module.route-table.rt-pvt_id
}

module "security-group" {
  source             = "./modules/SECURITY-GROUP"
  vpcid              = module.VPC.vpcid
  myip               = var.myip
  private_ip_bastion = module.ec2-instance.private_ip_bastion




}

module "ec2-instance" {
  source        = "./modules/EC2"
  pub_subnet_id = module.Subnet.public_subnet_id
  public_sg_id  = module.security-group.public_sg_id

  pvt_subnet_id = module.Subnet.private_subnet_id
  private_sg_id = module.security-group.private_sg_id
}

