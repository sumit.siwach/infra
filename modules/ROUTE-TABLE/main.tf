resource "aws_route_table" "rt-pub" {
  vpc_id = var.vpcid

   route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.igwid
   }
  
  tags = {
    Name = var.puclic_route_table
  }
}

resource "aws_route_table_association" "rt-pub_association" {
  count = length(var.public_subnet_name)
  subnet_id      = var.pub_subnet_id[count.index]
  route_table_id = var.rt-pub_id
}



resource "aws_route_table" "rt-pvt" {
  vpc_id = var.vpcid

   route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = var.ngwid
   }
  
  tags = {
    Name = var.private_route_table
  }
}

resource "aws_route_table_association" "rt-pvt_association" {
  count = length(var.private_subnet_name)
  subnet_id      = var.pvt_subnet_id[count.index]
  route_table_id = var.rt-pvt_id
}

