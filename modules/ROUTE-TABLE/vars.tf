variable "vpcid" {}

variable "igwid" {}
variable "ngwid" {}

variable "pub_subnet_id" {}
variable "pvt_subnet_id" {}


variable "rt-pub_id" {}
variable "rt-pvt_id" {}




variable "public_subnet_name" {
    description = "Name of public subnets"
    type    = list(string)
    default = ["sumit-pub-sub-01", "sumit-pub-sub-02"]
}

variable "private_subnet_name" {
    description = "Name of private subnets"
    type    = list(string)
    default = ["sumit-pvt-sub-01", "sumit-pvt-sub-02"]
}

variable "puclic_route_table" {
    description = "route table for public subnet"
    type    = string
    default = "sumit-route-pub"
}

variable "private_route_table" {
    description = "route table for private subnet"
    type    = string
    default = "sumit-route-pvt"
}

