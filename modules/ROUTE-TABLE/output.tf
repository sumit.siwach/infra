output "rt-pub_id" {
  value = aws_route_table.rt-pub.id
}

output "rt-pvt_id" {
  value = aws_route_table.rt-pvt.id
}

