resource "aws_vpc" "vpc_day9" {
  cidr_block  = var.vpccidr

  tags = {
    Name = var.vpcname
  }
}

