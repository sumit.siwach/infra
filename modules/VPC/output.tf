output "vpcid" {
  value = aws_vpc.vpc_day9.id
}

output "vpccidr" {
  value = aws_vpc.vpc_day9.cidr_block
}

