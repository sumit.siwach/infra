resource "aws_security_group" "public_sg" {
  name        = "sg_bastion_host"
  description = "Allow login from my IP only"
  vpc_id      = var.vpcid

  ingress {
    description      = "SSH from my IP only"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = [var.myip]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.pub_sg
  }
}


resource "aws_security_group" "private_sg" {
  name        = "sg_webserver"
  description = "Allow login from bastion hosts private ip only"
  vpc_id      = var.vpcid

  ingress {
    description      = "SSH from bastion host only"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["${var.private_ip_bastion[0]}/32"]

  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.pvt_sg
  }
}

