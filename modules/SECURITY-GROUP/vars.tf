variable "vpcid" {}
variable "myip" {}
variable "private_ip_bastion" {}


variable "pub_sg" {
    description = "name of public security group used for bastion host"
    type    = string
    default = "sg_bastion_host"
}

variable "pvt_sg" {
    description = "name of private security group used for private ec2 instance"
    type    = string
    default = "sg_webserver"
}

