resource "aws_instance" "public_ec2_instance" {
  count = length(var.public_ec2_instances)
  ami           = var.ami
  instance_type = "t2.micro"
  subnet_id = var.pub_subnet_id[count.index]
  vpc_security_group_ids = [var.public_sg_id]
  key_name = "tomcat-key"

  tags = {
    Name = var.public_ec2_instances[count.index]
  }
}


resource "aws_instance" "private_ec2_instance" {
  count = length(var.private_ec2_instances)
  ami           = var.ami
  instance_type = "t2.micro"
  subnet_id = var.pvt_subnet_id[count.index]
  vpc_security_group_ids = [var.private_sg_id]
  key_name = "tomcat-key"

  tags = {
    Name = var.private_ec2_instances[count.index]
  }
}

