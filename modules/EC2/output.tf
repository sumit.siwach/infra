output "public_ec2_instances" {
  value = aws_instance.public_ec2_instance[*].id
}

output "private_ip_bastion" {
  value = aws_instance.public_ec2_instance.*.private_ip
}

