variable "public_ec2_instances" {
    description = "Public EC2 instances"
    type    = list(string)
    default = ["sumit-ec2-pub-01+bastion" , "sumit-ec2-pub-02"]
}


variable "ami" {
    default = "ami-0851b76e8b1bce90b"
}

variable "pub_subnet_id" {}
variable "public_sg_id" {}



variable "private_ec2_instances" {
    description = "Private EC2 instances"
    type    = list(string)
    default = ["ninja-ec2-pvt-HA-01" , "ninja-ec2-pvt-HA-02"]
}

variable "pvt_subnet_id" {}
variable "private_sg_id" {}

