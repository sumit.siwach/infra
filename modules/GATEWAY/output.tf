output "igw-01_id" {
  value = aws_internet_gateway.igw-01.id
}

output "eip_id" {
  value = aws_eip.eip_01.id
}

output "ngw-01_id" {
  value = aws_nat_gateway.ngw-01.id
}

