resource "aws_internet_gateway" "igw-01" {
  vpc_id = var.vpc_id

  tags = {
    Name = var.internet_gateway
  }
}


resource "aws_eip" "eip_01" {
  vpc      = true
}

resource "aws_nat_gateway" "ngw-01" {
  allocation_id = var.eip_id
  subnet_id  = var.pub_subnet_id


  tags = {
    Name = var.nat_gateway
  }
}

