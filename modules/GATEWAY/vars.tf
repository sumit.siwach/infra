variable "vpc_id" {}

variable "eip_id" {}

variable "internet_gateway" {
    description = "Internet gateway"
    type    = string
    default = "sumit-igw-01"
}


variable "pub_subnet_id" {}


variable "nat_gateway" {
    description = "nat gateway for private subnet which needs  to be created in public subnet"
    type    = string
    default = "sumit-nat-01"
}

