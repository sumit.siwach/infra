resource "aws_subnet" "public_subnet" {
  count=length(var.public_subnet_name)
  vpc_id     = var.vpc_id
  cidr_block = cidrsubnet(var.vpc_cidr,8,count.index)
  availability_zone = var.availability_zones[count.index]
  map_public_ip_on_launch = true


  tags = {
    Name = var.public_subnet_name[count.index]
  }
}


resource "aws_subnet" "private_subnet" {
  count=length(var.private_subnet_name)
  vpc_id     = var.vpc_id
  cidr_block = cidrsubnet(var.vpc_cidr,8,count.index+length(var.public_subnet_name))
  availability_zone = var.availability_zones[count.index]

  tags = {
    Name = var.private_subnet_name[count.index]
  }
}

