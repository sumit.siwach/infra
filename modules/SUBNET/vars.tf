variable "vpc_id" {}

variable "vpc_cidr" {}

variable "public_subnet_name" {
    description = "Name of public subnets"
    type    = list(string)
    default = ["sumit-pub-sub-01", "sumit-pub-sub-02"]
}

variable "private_subnet_name" {
    description = "Name of private subnets"
    type    = list(string)
    default = ["sumit-pvt-sub-01", "sumit-pvt-sub-02"]
}

variable "availability_zones" {
    description = "AZs of Mumbai"
    type    = list(string)
    default = ["ap-south-1a", "ap-south-1b"]
}

