variable "vpccidrout" {
  description = "CIDR of VPC created"
  type        = string
  default     = "10.0.0.0/16"
}

variable "vpcnameout" {
  description = "Name of VPC created"
  type        = string
  default     = "sumit-vpc-01"
}

variable "myip" {
  description = "current public ip assigned to your system"
  type        = string
  default     = "103.206.51.155/32"
}

